# Laravel Vue Crowdfunding  Website Batch 37

## Name
Crowdfunding App

## Description
This is my final project for [Sanbercode](https://sanbercode.com/) Laravel + Vue js intensive bootcamp. It is a simple crowdfunding app that created using laravel 6 and vue.js v2, I learned how to combine vue.js and laravel, using rest api to send and request data from database, generate otp code and send it to email for email verification, and using google api to login our application.
The directory for the web application is on crowdfunding-website

## Technology
* [laravel 6](https://laravel.com/docs/6.x)
* [vue.js v2](https://v2.vuejs.org/) 
* [vue-router v3](https://v3.router.vuejs.org/)
* [vuex v3](https://v3.vuex.vuejs.org/)
* [vuetify v2](https://vuetifyjs.com/en/)
* [vuex-persist v3](https://www.npmjs.com/package/vuex-persist)
* [axios](https://github.com/axios/axios)
* [mailtrap](https://mailtrap.io/)

## Screenshot
here is the screenshot of the app\
[drive link](https://drive.google.com/drive/folders/1woz91bc8jHshHzxlmxXr_MNHREeCug2Y?usp=sharing)
<details><summary>Click to expand</summary>
1. sidebar user
<br>
2. sidebar guest
<br>
3. search dialog
<br>
4. login with google
<br>
5. login dialog
<br>
6. halaman home
<br>
7. detail campaign
<br>
8. berhasil donate
<br>
9. all campaigns
<br>
10. all blogs
<br>

<img src="https://drive.google.com/uc?export=view&id=1NiecdSZM0Bw1NPfn5o7r1lqx1WecapnK" align="left" width="200">
<img src="https://drive.google.com/uc?export=view&id=1hnhzT6k31JfgEOm9i8eSy5Zg7heiNGcx"  width="200">
<img src="https://drive.google.com/uc?export=view&id=1xXYlehptH4PamasWeP-xjpt8vGLpdTvz"  width="200">
<img src="https://drive.google.com/uc?export=view&id=1IpHiyVio9qjcYUDyuuFSGzHbPimGdv6A"  width="200">
<img src="https://drive.google.com/uc?export=view&id=1MP4PPSqqVBCR35RjRutCAa1SbQyoLaJb"  width="200">
<img src="https://drive.google.com/uc?export=view&id=1VOPPrDOIoNOI2c8AnKe_8pglqbPqvv-f"  width="200">
<img src="https://drive.google.com/uc?export=view&id=12SEuAcwkzg8i7uDJkGRqvTxEVW0mMY9s"  width="200">
<img src="https://drive.google.com/uc?export=view&id=11S-BBnKp8pJaOKD8-HHoU21UF-6CJlpV"  width="200">
<img src="https://drive.google.com/uc?export=view&id=143g6aXox1FjQcLJspztNfwghzqbbVguq"  width="200">
<img src="https://drive.google.com/uc?export=view&id=1fVLv2p780CaDxuP2l0ca9a0il9ip0eL8"  width="200">
</details>


## Demo Application
here is the link for demo application\
[video link](https://drive.google.com/file/d/1JOsq0oNi2BLhysOBqvKnNJyLXIGsztZ5/view?usp=sharing)
