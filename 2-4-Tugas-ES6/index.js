//soal 1
const luas = (panjang, lebar) => panjang * lebar;
const keliling = (panjang, lebar) => 2 * (panjang + lebar);

console.log(luas(5, 2)); // 10
console.log(keliling(5, 2)); // 14

//soal 2
// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName(){
      console.log(`${firstName} ${lastName}`)
    }
  }
}

//Driver Code 
newFunction("William", "Imoh").fullName();

//soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

const combined = [...west, ...east]

//Driver Code
console.log(combined)

//soal 5
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(before)