export const MemberComponent = {
  template: `
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Photo</th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">No. hp</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(member) in members" >
          <td><img :src="member.photo_profile ? 'http://demo-api-vue.sanbercloud.com' + member.photo_profile : null" alt="" class="img-fluid"></td>
          <td>{{ member.name }}</td>
          <td>{{ member.address }}</td>
          <td>{{ member.no_hp }}</td>
          <td>
            <div class="d-flex flex-column justify-content-between">
              <a class="btn btn-warning btn-sm" @click="$emit('edit', member)">Edit</a>
              <a class="btn btn-danger btn-sm" @click="$emit('remove', member.id)">Delete</a>
              <a class="btn btn-success btn-sm" @click="$emit('upload' ,member)">Upload</a>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  `,
  props: ['members']
}