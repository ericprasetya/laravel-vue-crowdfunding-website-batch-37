export const CounterComponent = {
  template: `
    <div> 
      <p> State pada component Counter {{ counter }} </p>
    </div> 
  `,
  computed : {
    counter() {
      return this.$store.getters.counter
    }
  }
}