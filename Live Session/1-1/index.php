<?php 
trait Mobil{
  public $merk;
  public $jalur = 500;
  public $jumlahRoda;
  public $keahlian;

  public function skill(){
    echo "{$this->merk} menggunakan {$this->keahlian}";
  }
}

abstract class Balapan{
  use Mobil;
  public $speedPower;
  public $heavyPower;
  public function mendahului($mobil){
    echo "{$this->merk} sedang menyusul {$mobil->merk}";
    $mobil->didahului($this);
  }
  public function didahului($mobil){
    echo "{$this->merk} sedang disusul {$mobil->merk}";
    $this->jalur = $this->jalur - ($mobil->speedPower/$mobil->heavyPower);
  }

  protected function getInfo(){
    echo "<br>";
    echo "Merk : {$this->merk} <br>";
    echo "Jumlah Roda : {$this->jumlahRoda} <br>";
    echo "Jalur : {$this->jalur} <br>";
    echo "Keahlian : {$this->keahlian} <br>";
    echo "Speed Power : {$this->speedPower} <br>";
    echo "Heavy Power : {$this->heavyPower} <br>";
    $this->skill();
  }
  abstract public function getInfoKendaraan();
}

class Tesla extends Balapan{
  public function __construct($merk)
  {
    $this->merk = $merk;
    $this->jumlahRoda = 4;
    $this->keahlian = "Mobil Listrik";
    $this->speedPower = 120;
    $this->heavyPower = 40;
  }
  public function getInfoKendaraan(){
    echo "Jenis Kendaraan : Tesla";
    $this->getInfo();
  }
}

class Mitsubishi extends Balapan{
  public function __construct($merk)
  {
    $this->merk = $merk;
    $this->jumlahRoda = 4;
    $this->keahlian = "Bensin";
    $this->speedPower = 200;
    $this->heavyPower = 30;
  }
  public function getInfoKendaraan(){
    echo "Jenis Kendaraan : Mitsubishi";
    $this->getInfo();
  }
}

$tesla = new Tesla("Tesla");
$tesla->getInfoKendaraan();
echo"<br> <br>";
$mitsubishi = new Mitsubishi("Mitsubishi");
$mitsubishi->getInfoKendaraan();
echo"<br> <br>";

$mitsubishi->mendahului($tesla);
echo"<br> <br>";


$tesla->getInfoKendaraan();
echo"<br> <br>";

$tesla->mendahului($mitsubishi);

$mitsubishi->getInfoKendaraan();

?>