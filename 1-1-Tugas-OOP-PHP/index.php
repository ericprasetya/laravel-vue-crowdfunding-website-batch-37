<?php 

trait Hewan{
  protected $nama;
  protected $darah = 50;
  protected $jumlahKaki;
  protected $keahlian;
  function atraksi(){
    echo "{$this->nama} sedang {$this->keahlian}";
  }
}

abstract class Fight{
  use Hewan;
  protected $attackPower, $defencePower;
  function serang($hewan){
    echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";
    $hewan->diserang($this);
  }

  function diserang($penyerang){
    echo "{$this->nama} sedang diserang";
    $this->darah = $this->darah - ($penyerang->attackPower/$this->defencePower);
  }
  protected function getInfo(){
    echo "<br>";
    echo "Nama : {$this->nama} <br>";
    echo "Darah : {$this->darah} <br>";
    echo "Jumlah Kaki : {$this->jumlahKaki} <br>";
    echo "Keahlian : {$this->keahlian} <br>";
    echo "Attack Power : {$this->attackPower} <br>";
    echo "Defence Power : {$this->defencePower} <br>";
  }
  abstract function getInfoHewan();
}

class Elang extends Fight{
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = 2;
    $this->keahlian = "terbang tinggi";
    $this->attackPower = 10;
    $this->defencePower = 5;
  }
  public function getInfoHewan(){
    echo "Jenis Hewan : Elang";
    $this->getInfo();
  }
}
class Harimau extends Fight{
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = 4;
    $this->keahlian = "lari cepat";
    $this->attackPower = 7;
    $this->defencePower = 8;
  }
  public function getInfoHewan(){
    echo "Jenis Hewan : Harimau";
    $this->getInfo();
  }
}

class Spasi{
  static function break(){
    echo "<br>";
  }
}

$harimau = new Harimau("Harimau_1");
$elang = new Elang("Elang_1");

$harimau->getInfoHewan();
Spasi::break();
$elang->getInfoHewan();
Spasi::break();

$harimau->serang($elang);
Spasi::break();

$harimau->getInfoHewan();
Spasi::break();
$elang->getInfoHewan();
Spasi::break();

$elang->serang($harimau);
Spasi::break();

$harimau->getInfoHewan();
Spasi::break();
$elang->getInfoHewan();
Spasi::break();
?>