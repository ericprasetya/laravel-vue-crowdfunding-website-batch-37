<?php

namespace App;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use UsesUuid;
    protected $fillable = ['name'];
    public function users()
    {
        return $this->hasMany(User::class, 'role_id');
    }
}
