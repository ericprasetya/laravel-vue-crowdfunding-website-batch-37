<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
  public function random($count){
    $blogs = Blog::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();
    $data['blogs'] = $blogs;
    return response()->json([
      'response_code' => '00',
      'response_message' => 'data blog berhasil ditampilkan',
      'data' => $data
    ], 200);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'title' => 'required',
      'description' => 'required',
      'image' => 'required|mimes:png,jpg,jpeg',
    ]);
    $blog = Blog::create([
      'title' => $request->title,
      'description' => $request->description,
    ]);
    if($request->hasFile('image')){
      $image = $request->file('image');
      $filename = $blog->id . '.' . $image->getClientOriginalExtension();
      $image_folder = '/photo/blog/';
      $image_location = $image_folder.$filename;

      try{
        $image->move(public_path('/photo/blog/'), $filename);
        $blog->update([
          'image' => $image_location,
        ]);
      }catch(\Exception $e){
        return response()->json([
          'response_code' => '01',
          'response_message' => 'gagal mengupload gambar blog',
        ],200);
      }

      $data['blog'] = $blog;
      return response()->json([
        'response_code' => '00',
        'response_message' => 'data blog berhasil ditambahkan',
        'data' => $data
      ], 200);
    }
  }

  public function index(){
    $blogs = Blog::paginate(6);
    $data['blogs'] = $blogs;
    return response()->json([
      'response_code' => '00',
      'response_message' => 'data blogs berhasil ditampilkan',
      'data' => $data
    ], 200);
  }
}
