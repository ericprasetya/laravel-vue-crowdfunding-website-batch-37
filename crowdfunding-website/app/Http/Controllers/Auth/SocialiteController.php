<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
          'url' => $url,
        ]);
    }
    public function handleProviderCallback($provider)
    {
      try{
        $social_user = Socialite::driver($provider)->stateless()->user();
        if(!$social_user){
          return response()->json([
            'response_code' => '01',
            'response_message' => 'login failed',
          ], 401);
        }
        $user = User::where('email', $social_user->getEmail())->first();
        if(!$user){
          if($provider == 'google'){
            $photo_profile = $social_user->avatar;
          }
          $user = User::create([
            'name' => $social_user->name,
            'email' => $social_user->email,
            'photo_profile' => $photo_profile,
            'email_verified_at' => Carbon::now(),
          ]);
        }
        $data['user'] = $user;
        $data['token'] = auth()->login($user);
        return response()->json([
          'response_code' => '00',
          'response_message' => 'login success',
          'data' => $data,
        ], 200);
      }catch(\Exception $e){
        return response()->json([
          'response_code' => '01',
          'response_message' => 'login failed',
        ], 401);
      }
    }
}
