<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationUserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp_code' => 'required',
        ]);

        $otp_code = OtpCode::where('otp', $request->otp_code)->first();
        if(!$otp_code){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Otp code tidak ditemukan'
            ], 400);
        }

        $now = Carbon::now();
        if($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Otp code sudah expired, silakan generate ulang'
            ], 400);
        }

        //update user bahwa sudah diverifikasi emailnya
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = $now;
        $user->save();

        //delete otp code
        $otp_code->delete();
        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'User berhasil diverifikasi',
            'data' => $data
        ], 200);

    }
}
