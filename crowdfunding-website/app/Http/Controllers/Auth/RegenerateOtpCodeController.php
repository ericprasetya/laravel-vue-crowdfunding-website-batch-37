<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpRegeneratedEvent;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
        ]);
        
        $user = User::where('email', $request->email)->first();
        $user->generateOtpCode();
        event(new OtpRegeneratedEvent($user));
        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Otp code berhasil di regenerate',
            'data' => $data
        ], 200);
    }
}
