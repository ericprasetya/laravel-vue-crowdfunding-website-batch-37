<?php

namespace App\Http\Controllers;

use App\Campaign;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function random($count){
      $campaigns = Campaign::select('*')
                      ->inRandomOrder()
                      ->limit($count)
                      ->get();
      $data['campaigns'] = $campaigns;
      return response()->json([
        'response_code' => '00',
        'response_message' => 'data campaign berhasil ditampilkan',
        'data' => $data
      ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'title' => 'required',
        'description' => 'required',
        'image' => 'required|mimes:png,jpg,jpeg',
        'address' => 'required',
        'required' => 'numeric',
        'collected' => 'numeric'
      ]);
      if(!$request->required){
        $request->required = 0;
      }
      if(!$request->collected){
        $request->collected = 0;
      }
      if($request->required < $request->collected){
        return response()->json([
          'response_code' => '01',
          'response_message' => 'dana terkumpul tidak boleh lebih besar dari dana yang dibutuhkan',
          'data' => null
        ], 200);
      }
      $campaign = Campaign::create([
        'title' => $request->title,
        'description' => $request->description,
        'required' => $request->required,
        'collected' => $request->collected,
      ]);
      if($request->hasFile('image')){
        $image = $request->file('image');
        $filename = $campaign->id . '.' . $image->getClientOriginalExtension();
        $image_folder = '/photo/campaign/';
        $image_location = $image_folder.$filename;

        try{
          $image->move(public_path('/photo/campaign/'), $filename);
          $campaign->update([
            'image' => $image_location,
          ]);
        }catch(\Exception $e){
          return response()->json([
            'response_code' => '01',
            'response_message' => 'gagal mengupload gambar campaign',
          ],200);
        }

        $data['campaign'] = $campaign;
        return response()->json([
          'response_code' => '00',
          'response_message' => 'data campaign berhasil ditambahkan',
          'data' => $data
        ], 200);
      }
    }

    public function index(){
      $campaigns = Campaign::paginate(6);
      $data['campaigns'] = $campaigns;
      return response()->json([
        'response_code' => '00',
        'response_message' => 'data campaigns berhasil ditampilkan',
        'data' => $data
      ], 200);
    }
    public function detail($id){
      $campaign = Campaign::find($id);
      $data['campaign'] = $campaign;
      return response()->json([
        'response_code' => '00',
        'response_message' => 'data campaign berhasil ditampilkan',
        'data' => $data
      ], 200);
    }

    public function search($keyword){
      $campaign = Campaign::select('*')
                      ->where('title', 'like', '%'.$keyword.'%')
                      ->get();
                      // ->orWhere('description', 'like', '%'.$keyword.'%')
                      // ->paginate(6);
      $data['campaigns'] = $campaign;
      return response()->json([
        'response_code' => '00',
        'response_message' => 'data campaigns berhasil ditampilkan',
        'data' => $data
      ], 200);
    }
}
