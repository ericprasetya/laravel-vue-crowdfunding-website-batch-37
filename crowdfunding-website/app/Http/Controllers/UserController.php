<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $user = User::where('id', $user->id)->first();
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'photo_profile' => 'required|image|file|max:5120',
        ]);

        if($file = $request->file('photo_profile')){
            if($user->photo_profile != null){
                // Storage::delete($user->photo_profile);
                File::delete(public_path($user->photo_profile));
            }
            $target_path = '/photo/users/photo-profile/';
            $filename = Str::slug($user->name, '-').'-'.$user->id.'.'.$file->getClientOriginalExtension();
            $file->move(public_path($target_path), $filename);
            $validatedData['photo_profile'] = $target_path.$filename;
            // $username = $user->name;
            // $username = explode(' ', $username);
            // $username = implode('-', $username);
            // $validatedData['photo_profile'] = $request->photo_profile->storeAs('public/photo/users/photo-profile', $username.'-'.time().'.'.$file->getClientOriginalExtension());
        }
        $user->update($validatedData);
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil di update',
            'data' => $user,
            'file' => $request->photo_profile
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|string|min:6',
            'new_password' => 'required|string|min:6',
        ]);
        $credentials = $request->only('email', 'password');
        if (auth()->validate($credentials)) {
            // $user = User::where('email', $request->email)->first();
            // $user->password = bcrypt($request->new_password);
            // $user->save();
            User::where('email', $request->email)->update(['password' => bcrypt($request->new_password)]);
            return response()->json([
                'response_code' => '00',
                'response_message' => 'Password berhasil diubah',
            ], 200);
        }
        return response()->json([
            'error' => 'Credentials salah',
        ], 401);
    }

    public function getProfile(){
        $user = auth()->user();

        return response()->json([
            'response_code' => '00',
            'data' => $user,
        ], 200);
    }
}
