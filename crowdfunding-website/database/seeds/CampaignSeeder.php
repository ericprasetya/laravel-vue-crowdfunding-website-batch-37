<?php

use App\Campaign;
use Illuminate\Database\Seeder;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campaign::create([
          'title' => 'campaign bantuan 1',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 0,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 2',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 10,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 3',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 220,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 4',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 1500000,
          'collected' => 0,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 5',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 120000,
          'collected' => 100000,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 6',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 3000,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 7',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 0,
        ]);
        Campaign::create([
          'title' => 'campaign bantuan 8',
          'description' => 'bantuan untuk mereka yang membutuhkan',
          'address' => 'Jl. Kebon Kacang',
          'required' => 100000,
          'collected' => 120000,
        ]);
    }
}
