<?php

use App\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Blog::create([
          'title' => 'blog 1',
          'description' => 'blog 1 description'
        ]);

        Blog::create([
          'title' => 'blog 2',
          'description' => 'blog 2 description'
        ]);

        Blog::create([
          'title' => 'blog 3',
          'description' => 'blog 3 description'
        ]);

        Blog::create([
          'title' => 'blog 4',
          'description' => 'blog 4 description'
        ]);
    }
}
