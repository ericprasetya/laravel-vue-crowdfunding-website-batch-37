<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
  'prefix' => 'auth',
  'namespace' => 'Auth'
], function(){
  Route::post('register', 'RegisterController');
  Route::post('regenerate-otp-code', 'RegenerateOtpCodeController');
  Route::post('verification', 'VerificationUserController');
  Route::post('login', 'LoginController');
  Route::post('logout', 'LogoutController')->middleware('auth:api');
  Route::post('check-token', 'CheckTokenController')->middleware('auth:api');

  Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
  Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
  
});

Route::post('auth/update-password', 'UserController@updatePassword');

Route::group([
  'middleware' => ['api', 'profile', 'email_verification', 'auth:api'],
], function(){
  Route::post('profile/show', 'UserController@update');
  Route::get('profile/update', 'UserController@getProfile');
});

Route::group([
'middleware' => ['api'],
  'prefix' => 'campaign',
], function(){
  Route::get('random/{count}', 'CampaignController@random');
  Route::post('store', 'CampaignController@store')->middleware('auth:api');
  Route::get('/', 'CampaignController@index');
  Route::get('/{id}', 'CampaignController@detail');
  Route::get('search/{keyword}', 'CampaignController@search');
});

Route::group([
  'middleware' => ['api'],
  'prefix' => 'blog',
], function(){
  Route::get('random/{count}', 'BlogController@random');
  Route::post('store', 'BlogController@store');
  Route::get('/', 'BlogController@index');
});