<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/{any?}', function () {
    return view('app');
})->where('any', '.*');

// Route::get('/test', function () {
//   return view('send_email_user')->with([
//     "message" => "Selamat otp Bapak/Ibu berhasil diregenerate.",
//     "otp" => "123456",
//   ]);
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/route-1', function () {
//     return 'email sudah terverifikasi';
// })->middleware('email_verification');

// Route::get('/route-2', function () {
//     return 'welcome admin';
// })->middleware(['email_verification', 'admin']);