export default{
  namespaced: true,
  state: {
    status: false,
    component: 'search', //login, search, dll
  },
  mutations: {
    setStatus(state, payload){
      state.status = payload
    },
    setComponent(state, payload){
      state.component = payload
    }
  },
  actions: {
    setStatus({commit}, payload){
      commit('setStatus', payload)
    },
    setComponent({commit}, payload){
      commit('setComponent', payload)
      commit('setStatus', true)
    },
  },
  getters: {
    status : state => state.status,
    component : state => state.component,
  },
}