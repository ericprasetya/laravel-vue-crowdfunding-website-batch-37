export default {
  namespaced: true,
  state: {
    transactions: 0,
  },
  mutations: {
    addTransaction(state){
      state.transactions++
    }
  },
  getters: {
    transactions(state){
      return state.transactions
    }
  },
  actions: {

  }
}